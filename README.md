# BrasilPrev Challenge #

## Global Details ##
I'm sending the challenge that the company proposed to me
There are two repositories, as my profile is FullStack I created two
One for the frontend and one for the backend, both can be accessed by commands
There are README.md files that document the two fronts

### FrontEnd - Angular Solution 7 ###
FrontEnd: Application aimed at creating Angular 7 components of the solution, using Bootstrap, CSS, Typescript.
With some communication resources to the micro services developed in SpringBoot in the backend layer
FrontEnd Bitbucket: git clone https: //jairodealmeida@bitbucket.org/jairodealmeida/brasilprev-front.git
The server I used for the Cloud on the landing, Angular 7 is on Heroku
Angular Front: https://stormy-journey-60924.herokuapp.com/

### PROD deploy package.json ###
Anotation to make prod deploy
```js
...
"engines": {
  "node": "12.x"
},
...
"scripts": {
  "postinstall": "ng build --aot --prod",
}
...

```
### Components ###
Class that comes from a serializable object from the frontend, has hash and compare attributs

## To remote DEV developers ##

Command to execute no instalation of npm libraries
```
npm install
```

Command to execute angular dev mode

```
ng serve
```

OR if you have the prod
```
npm run postinstall
```

Other option is run
 from '@angular/core/testing';
 About this
 ```
 npm run test
 ```




## Heroku Annotations Steps ##

* Install Heroku Client
* Commit changes to Git end Push       
* heroku create  (Create heroku cloud )
* heroku logs --tail     (Log It)
* heroku open (Open remote app)
* git push heroku master (Push changes to Heroky GIt and Autodeploy it)
* heroku ps:scale web=1 (Define Web)

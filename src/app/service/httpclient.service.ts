import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Customer{
  constructor(
    public id:string,
    public name:string,
    public registry:string,
    public address:string
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient:HttpClient
  ) {
     }
  getAll()
  {
    return this.httpClient.get<Customer[]>('http://localhost:8080/customers');
  }

  public delete(model) {
    return this.httpClient.delete<Customer>("http://localhost:8080/customers" + "/"+ model.id);
  }

  public create(model) {
    return this.httpClient.post<Customer>("http://localhost:8080/customers", model);
  }





}

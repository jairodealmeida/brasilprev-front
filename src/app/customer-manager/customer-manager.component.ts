import { Component, OnInit } from '@angular/core';
import { HttpClientService, Customer } from '../service/httpclient.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-customer-manager',
  templateUrl: './customer-manager.component.html',
  styleUrls: ['./customer-manager.component.css']
})
export class CustomerManagerComponent implements OnInit {
  // id, name, registry , adreess

  user: Customer = new Customer(this.generator(),"","","");
  public ids: string[] = [];
  constructor(
    private httpClientService: HttpClientService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  create(): void {
    this.httpClientService.create(this.user)
        .subscribe( data => {

          this.router.navigate(['/']);
        });

  };

  private generator(): string {
      const isString = `${this.S4()}${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}${this.S4()}${this.S4()}`;

      return isString;
    }

    private idExists(id: string): boolean {
      return this.ids.includes(id);
    }

    private S4(): string {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
}

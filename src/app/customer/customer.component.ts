import { Component, OnInit } from '@angular/core';
import { HttpClientService, Customer } from '../service/httpclient.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  customers:Customer[];


  constructor(
    private httpClientService:HttpClientService
  ) { }

  ngOnInit() {
     this.httpClientService.getAll().subscribe(
      response =>this.handleSuccessfulResponse(response),
     );
  }

handleSuccessfulResponse(response)
{
    this.customers=response;
}

delete(model: Customer): void {
   this.httpClientService.delete(model)
     .subscribe( data => {
      this.customers = this.customers.filter(u => u !== model);
   })
};


}
